from django.test import Client, TestCase

class TestStory5(TestCase):
    def test_galeri(self):
        response = Client().get('/gallery/')
        self.assertEquals(response.status_code, 200)
    def test_favorit(self):
        response = Client().get('/favorites/')
        self.assertEquals(response.status_code, 200)
    def test_data(self):  
        response = Client().get('/data/')
        self.assertEquals(response.status_code, 200)   
    def test_index_views_pakai_template(self):
        response = self.client.get('/')
        self.assertTemplateUsed(response, 'index.html')