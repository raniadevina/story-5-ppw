from django import forms

class FormMatkul(forms.Form):

    TAHUN = (
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2021/2022', 'Gasal 2021/2022'),
        ('Genap 2021/2022', 'Genap 2021/2022'),
    )

    nama_matkul = forms.CharField(label="Mata Kuliah", widget=forms.TextInput(
        attrs={
            'class':'form-control',
            'placeholder':'Enter Subject Here',
            'required':True,
        }
    )
)

    jumlah_sks = forms.IntegerField(
        label = "Jumlah SKS",
        widget = forms.NumberInput(
            attrs={
                'class':'form-control',
                'placeholder':'Enter Credits',
                'required':True,
                'min':'1',
                'max':'6'
            }
        )
    )

    semester = forms.CharField(
        label="Semester", 
        widget=forms.Select(choices=TAHUN)
    )

    ruangan = forms.CharField(
        label = "Ruang Kelas", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Enter your room',
                'required':True
            }
        )
    )

    dosen = forms.CharField(
        label = "Dosen Pengampu", 
        max_length = 100,
        widget = forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Enter here',
                'required':True
            }
        )
    )

    deskripsi = forms.CharField(
        label = "Deskripsi mata kuliah", 
        max_length = 100,
        widget = forms.Textarea(
            attrs={
                'class':'form-control',
                'placeholder':'Describe the subject',
                'required':True
            }
        )
    )

