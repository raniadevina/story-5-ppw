from django.db import models

# Create your models here.
class MataKuliah(models.Model):
    TAHUN = [
        ('Gasal 2019/2020', 'Gasal 2019/2020'),
        ('Genap 2019/2020', 'Genap 2019/2020'),
        ('Gasal 2020/2021', 'Gasal 2020/2021'),
        ('Genap 2020/2021', 'Genap 2020/2021'),
        ('Gasal 2021/2022', 'Gasal 2021/2022'),
        ('Genap 2021/2022', 'Genap 2021/2022'),
    ]

    nama_matkul = models.CharField(max_length = 100)
    jumlah_sks = models.IntegerField()
    ruangan = models.CharField(max_length = 100)
    dosen = models.CharField(max_length = 100)
    semester = models.CharField(max_length = 100, choices=TAHUN )
    deskripsi = models.CharField(max_length = 1000, null = True)

    def __str__(self):
        return self.nama_matkul

